import numpy as np
import matplotlib.pyplot as plt


def check_align(pt1,pt2,pt3):
        #pt1,pt2,pt3 aligned if vec(pt2-pt1) // vec(pt3-pt1) -> |vec1|.|vec2|= +1 or -1
        vec1 = np.array(pt2) - np.array(pt1)
        vec2 = np.array(pt3) - np.array(pt1)
    
        prod = (1./np.linalg.norm(vec1))*(1./np.linalg.norm(vec2))*np.dot(vec1,vec2)
        return abs(prod)==1.

def circular_shuffle(lst):
    if not lst:
        return lst
    
    first_element = lst[0]
    lst.pop(0)  # Remove the first element
    lst.append(first_element)
    return lst


class closed_shape_grid:
    l_ptm = []
    def __init__(self,lx,ly,dx,dy):
        self.lx = lx
        self.ly = ly
        
        #Create bounding box
        xmin = min(lx)
        xmax = max(lx)
    
        ymin = min(ly)
        ymax = max(ly)
    
        self.lxmesh = np.arange(((int(xmin/dx)-1.)*dx),((int(xmax/dx)+2.)*dx),dx).round(3)
        self.lymesh = np.arange(((int(ymin/dy)-1.)*dy),((int(ymax/dy)+2.)*dy),dy).round(3)
    
    def set_lx(self,lx):
        self.lx = lx
    
    def set_lx(self,ly):
        self.ly = ly
        
    def set_lxmesh(self,lxmesh):
        self.lxmesh = lxmesh

    def set_lymesh(self,lymesh):
        self.lymesh = lymesh
    
    #Node
    def get_nearest_pt(self,x,y):
        xm = self.lxmesh[np.abs(np.array(self.lxmesh)-x).argmin()]
        ym = self.lymesh[np.abs(np.array(self.lymesh)-y).argmin()]
        return xm,ym
        
    #Edge
    def get_ptm(self):
        #Snap point to grid
        self.l_ptm = []
        for x,y in zip(self.lx,self.ly):
            xm,ym = self.get_nearest_pt(x,y)
            ptm = [xm,ym]
            if ptm not in self.l_ptm:
                self.l_ptm.append(ptm)
        return self.l_ptm
    
    def clean_edge(self):
        itmax = 20
        l_ptmok = list(self.l_ptm)
        it = 0
        N = len(self.l_ptm)
    
        while it < itmax:
            N1 = len(l_ptmok)
            to_remove = []
            #N1 -> 2*N1 : to loop in l_ptm
            for i in range(2*N1):
                pt1 = l_ptmok[i%N1]
                pt2 = l_ptmok[(i + 1) % N1]
                pt3 = l_ptmok[(i + 2) % N1]
            
                if check_align(pt1, pt2, pt3):
                    to_remove.append(pt2)
        
            l_ptmok = [pt for pt in l_ptmok if pt not in to_remove]
            it += 1
            N2 = N1 - len(to_remove)
            if (N1 == N2):
                break
    
        self.l_ptm = l_ptmok  # Update the original l_ptm with cleaned points
        return self.l_ptm
    
    def get_fds_border(self,z1=0,z2=0.5,surf_id='surf1',bndf='.TRUE.'):
        l_ptmok = list(self.l_ptm)
        l_ptmok.append(l_ptmok[0])
        for i in range(len(l_ptmok)-1):
            x1,y1 = l_ptmok[i]
            x2,y2 = l_ptmok[i+1]
        
            x1ok = min(x1,x2)
            x2ok = max(x1,x2)
        
            y1ok = min(y1,y2)
            y2ok = max(y1,y2)
            print(f"&OBST XB = {x1ok},{x2ok},{y1ok},{y2ok},{z1},{z2}, SURF_ID = '{surf_id}',BNDF_OBST = {bndf}/ ")
            
    def view_boundary(self):
        fig, ax = plt.subplots()
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
    
        ax.set_xticks(self.lxmesh)
        ax.set_yticks(self.lymesh)
        #ax.grid(True)
    
        ax.plot(self.lx,self.ly,label='Original')
        self.lxm = np.array(self.l_ptm).transpose()[0]
        self.lym = np.array(self.l_ptm).transpose()[1]
        ax.plot(self.lxm,self.lym,label='Modified')
        ax.axis('equal')
        ax.legend(loc='best')
        plt.show()


R = 7.
#K -> Control density of points
K = 1500.
N = int(K*R)
theta = np.linspace(0,2*np.pi,N)
lx = R*np.cos(theta)
ly = R*np.sin(theta)

dx = 0.5
dy = 0.5

csg = closed_shape_grid(lx,ly,dx,dy)
csg.get_ptm()
l_ptm = csg.clean_edge()
csg.view_boundary()

z1 = 0.
z2 = 10.

surf_id = 'SILO_ACIER'
bndf = '.TRUE.'

csg.get_fds_border(z1,z2,surf_id,bndf)